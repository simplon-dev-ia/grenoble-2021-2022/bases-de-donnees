# Bases de données - Initiation

Cette première activité est une initiation aux bases de données.

## Recherche d'information

Avant de commencer à manipuler, faisons une rapide veille technologique sur le sujet :

- [ ] Qu'est-ce qu'une base de données ?
- [ ] Quelles sont les grandes familles de bases de données ?
- [ ] Quels sont les cas d'usage de chacune de ces familles ?
- [ ] À quelle famille appartient SQLite ? Trouver d'autres exemples de la même famille
- [ ] Quels sont les usage de SQLite ?
- [ ] Qu'est-ce que le langage SQL ?
- [ ] Quels sont les éléments principaux manipulables avec le langage SQL ?

## Installation de SQLite

Nous allons utiliser la base de données [SQLite][sqlite] pour cette activité.
Cette base de données a pour particularité de stocker ses données dans un seul
fichier binaire et d'être extrêmement simple à mettre en oeuvre.

- [ ] Installer le programme `sqlite3`
```bash
sudo apt-get install sqlite3
```
- [ ] Vérifier que l'installation est fonctionnelle en démarrant le programme `sqlite3` :
```bash
sqlite3
```

## Création d'une base de données SQLite

Nous allons réutiliser le jeu de données des [_demandes de valeurs foncières géolocalisées_][dvf]
founies par le gouvernement français : tentons d'analyser le [jeu complet][dvf-dataset] des DVF 2021
pour toute la France.

Le jeu de données est au format CSV, contient plus d'un million de lignes et
pèse environ 200 Mo. Analyser ce jeu avec un tableur devient rapidement une
opération complexe, utilisons donc une base de données !

### Jeu de données initial

Récupérons déjà notre jeu de données initial :

- [ ] Télécharger le jeu de données compressé [`full.csv.gz`][dvf-dataset]
- [ ] Décompresser le fichier `full.csv`
- [ ] Vérifier l'intégrité du fichier `full.csv` en affichant ses 10 premières lignes
- [ ] Prendre note du format CSV utilisé : quel est le séparateur de colonnes utilisé ?
- [ ] Compter le nombre de ligne du fichier CSV

### Création de la base de données

Créons une base de données SQLite à partir de ce jeu de données CSV :

- [ ] Créer une base de données SQLite nommée `dvf.db`:
```bash
sqlite3 dvf.db
```
- [ ] Créer une table `dvf` avec les colonnes identiques au CSV :
```sql
CREATE TABLE dvf(
  "id_mutation" TEXT,
  "date_mutation" TEXT,
  "numero_disposition" TEXT,
  "nature_mutation" TEXT,
  "valeur_fonciere" REAL,
  "adresse_numero" TEXT,
  "adresse_suffixe" TEXT,
  "adresse_nom_voie" TEXT,
  "adresse_code_voie" TEXT,
  "code_postal" TEXT,
  "code_commune" TEXT,
  "nom_commune" TEXT,
  "code_departement" TEXT,
  "ancien_code_commune" TEXT,
  "ancien_nom_commune" TEXT,
  "id_parcelle" TEXT,
  "ancien_id_parcelle" TEXT,
  "numero_volume" TEXT,
  "lot1_numero" TEXT,
  "lot1_surface_carrez" REAL,
  "lot2_numero" TEXT,
  "lot2_surface_carrez" REAL,
  "lot3_numero" TEXT,
  "lot3_surface_carrez" REAL,
  "lot4_numero" TEXT,
  "lot4_surface_carrez" REAL,
  "lot5_numero" TEXT,
  "lot5_surface_carrez" REAL,
  "nombre_lots" INTEGER,
  "code_type_local" TEXT,
  "type_local" TEXT,
  "surface_reelle_bati" INTEGER,
  "nombre_pieces_principales" INTEGER,
  "code_nature_culture" TEXT,
  "nature_culture" TEXT,
  "code_nature_culture_speciale" TEXT,
  "nature_culture_speciale" TEXT,
  "surface_terrain" INTEGER,
  "longitude" REAL,
  "latitude" REAL
);
```
- [ ] Activer le mode CSV pour SQLite:
```sql
.mode csv
```
- [ ] Spécifier le séparateur CSV utilisé dans le jeu de données (ici, `,`):
```sql
.separator ","
```
- [ ] Importer le fichier CSV dans une nouvelle table nommée `dvf`:
```sql
.import --skip 1 "full.csv" dvf

# si l'option --skip n'est pas disponible:
# .import '| tail -n +2 full.csv' dvf
```
- [ ] Vérifier que la table `dvf` a bien été créée:
```sql
.tables
.schema dvf
```
- [ ] Vérifier que les données ont bien été importées en affichant les 10 premières lignes
      avec cette requête SQL (nous verrons juste après comment la construire):
```sql
SELECT * FROM dvf LIMIT 10;
```
- [ ] Quitter la base de données:
```sql
.quit
```

## Exploitation de données avec SQL

Maintenant que nous avons une base de données, nous pouvons commencer à exploiter les données !

Pour interroger les données d'une base SQLite, nous devons utiliser le langage SQL.
Pas d'inquiétude, ce langage est relativement simple par sa nature déclarative :
vous exprimez quelles données extraire, la base de données s'occupe de vous l'apporter !

### Exploration préliminaire

En guise de démarrage, nous allons commencer par calculer quelques statistiques
sur notre jeu de données grâce au langage SQL.

- [ ] Calculer le nombre total de ligne stockées dans la table `dvf` :
```sql
SELECT count(*)
FROM dvf;
```
- [ ] Calculer la valeur foncière moyenne pour toute la table :
```sql
SELECT avg(valeur_fonciere)
FROM dvf;
```
- [ ] Afficher les valeurs distinctes du type de local :
```sql
SELECT DISTINCT type_local
FROM dvf;
```
- [ ] Afficher le nombre de nom de communes distinctes :
```sql
SELECT count(DISTINCT nom_commune)
FROM dvf;
```
- [ ] Afficher la valeur foncière moyenne pour la commune de Grenoble :
```sql
SELECT avg(valeur_fonciere)
FROM dvf
WHERE nom_commune = 'Grenoble';
```
- [ ] Afficher la valeur foncière moyenne par type de local :
```sql
SELECT type_local, avg(valeur_fonciere)
FROM dvf
GROUP BY type_local;
```
- [ ] Afficher la valeur foncière moyenne par type de local pour la commune de Grenoble ordonné par la valeur foncière :
```sql
SELECT type_local, avg(valeur_fonciere)
FROM dvf
WHERE nom_commune = 'Grenoble'
GROUP BY type_local
ORDER BY avg(valeur_fonciere);
```
- [ ] Afficher le nombre de ventes par mois :
```sql
SELECT strftime('%Y-%m', date_mutation), count(*)
FROM dvf
GROUP BY strftime('%Y-%m', date_mutation);
```

Nous venons de passer en revue les éléments principaux des requêtes SQL :

- Sélection de colonnes avec l'instruction `SELECT`
- Sélection de table avec l'instruction `FROM`
- Filtrage de lignes avec l'instruction `WHERE`
- Groupement de lignes avec l'instruction `GROUP BY`
- Limiter le nombre de résultats avec l'instruction `LIMIT`
- Ordonner les résultats avec l'instruction `ORDER BY`

### Exécution d'une requête externe

SQLite vous permet d'exécuter une requête SQL stockée dans un fichier.

- [ ] Créer un fichier vide `ventes_par_mois.sql`
- [ ] Ajouter dans le fichier la requête SQL pour afficher le nombre de ventes par mois
- [ ] Dans un shell SQLite, exécuter la requête :
```sql
.read ventes_par_mois.sql
```

### Exercices

Trouver les requêtes SQL pour répondre aux questions suivantes :

- [ ] Quelles sont les 10 communes ayant le prix au mètre carré moyen d'une maison le plus élevé ?
- [ ] Combien de ventes ont été enregistrées pour le département de l'isère au mois d'avril 2021 ?
- [ ] Combien de ventes ont été enregistrées pour chaque département de la région Auvergne Rhone Alpes au mois de mars 2021 ?
- [ ] Quelles sont les 20 communes de l'Isère ayant le prix au mètre carré moyen d'un appartement le moins élevé ?

## Pour aller plus loin

Cette activité n'est qu'une initiation à l'utilisation de bases de données
et au langage SQL.

Pour découvrir et s'entrainer avec le langage SQL, vous pouvez utiliser la plateforme
[SQLBolt][sqlbolt].

[sqlite]: https://sqlite.org "SQLite"
[dvf]: https://www.data.gouv.fr/fr/datasets/demandes-de-valeurs-foncieres-geolocalisees "DVF"
[dvf-dataset]: https://files.data.gouv.fr/geo-dvf/latest/csv/2021/full.csv.gz
[sqlbolt]: https://sqlbolt.com "SQLBolt"
