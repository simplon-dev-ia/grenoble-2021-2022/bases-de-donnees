# Bases de données - Modélisation Analytique

Cette seconde activité est une initiation à la modélisation de données analytique.

Dans la première activité, nous travaillé avec une base de données contenant une
table unique `dvf`. Toutes les requêtes d'exploitation étaient effectuées contre
cette table. Pour les colonnes numériques, cela ne pose aucun problème. En revanche,
pour les colonnes catégorielles, il devient rapidement complexe d'effectuer des
opérations car il faut en permanence extraire les valeurs uniques (les catégories).

La modélisation de données analytique est une technique permettant d'indexer les
catégories dans leurs tables séparées et d'y faire référence dans la table centrale.
Cela permet notamment aux analystes de pouvoir filtrer, grouper et trier les données
par des paramètres explicites.

Cette activité est à réaliser en binôme, mais chaque apprenant doit implémenter les
transformations.

Vocabulaire :

- Le type de modélisation que nous allons utiliser est un _schéma en étoile_ (_star schema_)
- La table de données centrale est appelée un _fait_ ou _événement_ (_fact_)
- Les tables contenant les valeurs uniques des catégories sont appelées des _dimensions_

## Modélisation de Données

Voici un exemple de diagramme _entité-relation_ utilisant la modélisation de données
en étoile :

![Exemple Star Schema](starschema.svg)

Ce diagramme modélise les entités suivantes :

- 3 tables de dimensions (`Browser`, `System`, `Location`) contenant des caractéristiques et une _clé primaire_
- 1 table de fait (`Event Log`) avec 3 _clés étrangères_ liés aux dimensions (`browser_id`, `system_id`, `location_id`) et 2 attributs internes (`timestamp`, `value`)
- 3 relations _1-plusieurs_ (_one-to-many_) entre la table de fait et les tables de dimensions

Les liaisons entre les tables utilisent les entités suivantes :

- **Clé primaire** : identifiant unique d'une entité
- **Clé étrangères** : identifiant faisant référence à une autre entité

### Exercice

En reprenant le jeu de données des DVF de l'activité 1, réaliser un diagramme entité-relation
comme-ci dessus, avec 1 table de fait et plusieurs tables dimensions.

**Conseil** : avant de construire le diagramme, tenter d'identifier les colonnes qui seraient
candidates à devenir des dimensions !

## Transformation de la base de données

Nous allons maintenant transformer notre base de données des DVF mono-table
en une base de données avec une organisation en étoile.

Nous avons identifié la colonne `type_local` comme étant candidate à devenir une dimension
puisque ne contenant que 5 valeurs distinctes.

Nous allons donc passer de la table `dvf` unique vers une nouvelle organisation :

- 1 table `fact_dvf`
- 1 table `dim_type_local`

![DVF Star Schema 1](dvfstarschema1.svg)

### Table de dimension

Voici les étapes pour créer et remplir la table de dimension `dim_type_local` :

- [ ] Afficher les valeurs distinctes de la colonne `type_local` :
```sql
SELECT DISTINCT type_local
FROM dvf;
```
- [ ] Créer une table de dimension `dim_type_local` :
```sql
CREATE TABLE dim_type_local(
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "nom" TEXT
);
```
- [ ] Remplir la table de dimension `dim_type_local` avec les valeurs unique de la colonne `type_local` :
```sql
INSERT INTO dim_type_local(nom)
SELECT DISTINCT type_local
FROM dvf;
```
- [ ] Vérifier que la table de dimension `dim_type_local` contient bien uniquement les valeurs distinctes :
```sql
SELECT *
FROM dim_type_local;
```

### Table de fait

Voici les étapes pour créer et remplir la table de fait `fact_dvf`,
à lier avec la table de dimension `dim_type_local` :

- [ ] Calculer le nombre de ligne de la table `dvf` :
```sql
SELECT count(*)
FROM dvf;
```
- [ ] Créer la table de fait `fact_dvf` :
```sql
CREATE TABLE fact_dvf(
  "id_mutation" TEXT,
  "date_mutation" TEXT,
  "numero_disposition" TEXT,
  "nature_mutation" TEXT,
  "valeur_fonciere" REAL,
  "adresse_numero" TEXT,
  "adresse_suffixe" TEXT,
  "adresse_nom_voie" TEXT,
  "adresse_code_voie" TEXT,
  "code_postal" TEXT,
  "code_commune" TEXT,
  "nom_commune" TEXT,
  "code_departement" TEXT,
  "ancien_code_commune" TEXT,
  "ancien_nom_commune" TEXT,
  "id_parcelle" TEXT,
  "ancien_id_parcelle" TEXT,
  "numero_volume" TEXT,
  "lot1_numero" TEXT,
  "lot1_surface_carrez" REAL,
  "lot2_numero" TEXT,
  "lot2_surface_carrez" REAL,
  "lot3_numero" TEXT,
  "lot3_surface_carrez" REAL,
  "lot4_numero" TEXT,
  "lot4_surface_carrez" REAL,
  "lot5_numero" TEXT,
  "lot5_surface_carrez" REAL,
  "nombre_lots" INTEGER,
  "code_type_local" TEXT,
  "surface_reelle_bati" INTEGER,
  "nombre_pieces_principales" INTEGER,
  "code_nature_culture" TEXT,
  "nature_culture" TEXT,
  "code_nature_culture_speciale" TEXT,
  "nature_culture_speciale" TEXT,
  "surface_terrain" INTEGER,
  "longitude" REAL,
  "latitude" REAL,
  "dim_type_local_id" INTEGER,
  FOREIGN KEY([dim_type_local_id]) REFERENCES dim_type_local(id)
);
```
- [ ] Inserer les lignes transformées dans la nouvelle table de fait `fact_dvf` :
```sql
INSERT INTO fact_dvf(
  id_mutation,
  date_mutation,
  numero_disposition,
  nature_mutation,
  valeur_fonciere,
  adresse_numero,
  adresse_suffixe,
  adresse_nom_voie,
  adresse_code_voie,
  code_postal,
  code_commune,
  nom_commune,
  code_departement,
  ancien_code_commune,
  ancien_nom_commune,
  id_parcelle,
  ancien_id_parcelle,
  numero_volume,
  lot1_numero,
  lot1_surface_carrez,
  lot2_numero,
  lot2_surface_carrez,
  lot3_numero,
  lot3_surface_carrez,
  lot4_numero,
  lot4_surface_carrez,
  lot5_numero,
  lot5_surface_carrez,
  nombre_lots,
  code_type_local,
  surface_reelle_bati,
  nombre_pieces_principales,
  code_nature_culture,
  nature_culture,
  code_nature_culture_speciale,
  nature_culture_speciale,
  surface_terrain,
  longitude,
  latitude,
  dim_type_local_id
)
SELECT
  id_mutation,
  date_mutation,
  numero_disposition,
  nature_mutation,
  valeur_fonciere,
  adresse_numero,
  adresse_suffixe,
  adresse_nom_voie,
  adresse_code_voie,
  code_postal,
  code_commune,
  nom_commune,
  code_departement,
  ancien_code_commune,
  ancien_nom_commune,
  id_parcelle,
  ancien_id_parcelle,
  numero_volume,
  lot1_numero,
  lot1_surface_carrez,
  lot2_numero,
  lot2_surface_carrez,
  lot3_numero,
  lot3_surface_carrez,
  lot4_numero,
  lot4_surface_carrez,
  lot5_numero,
  lot5_surface_carrez,
  nombre_lots,
  code_type_local,
  surface_reelle_bati,
  nombre_pieces_principales,
  code_nature_culture,
  nature_culture,
  code_nature_culture_speciale,
  nature_culture_speciale,
  surface_terrain,
  longitude,
  latitude,
  dim_type_local.id
FROM dvf
JOIN dim_type_local ON dvf.type_local = dim_type_local.nom;
```
- [ ] Vérifier le nombre de lignes insérées dans la table `fact_dvf` :
```sql
SELECT count(*)
FROM fact_dvf;
```
- [ ] Vérifier les valeurs uniques de la clé étrangère `dim_type_local_id` :
```sql
SELECT DISTINCT dim_type_local_id
FROM fact_dvf;
```

### Exploitation Analytique

Pour interroger notre nouvelle structure, nous allons utiliser la notion de **jointure**
entre notre table de fait `fact_dvf` et notre table de dimension `dim_type_local`.

Par exemple, pour calculer la valeur foncière moyenne par type de local :

```sql
SELECT dim_type_local.nom, avg(valeur_fonciere)
FROM fact_dvf
JOIN dim_type_local ON dim_type_local.id = fact_dvf.dim_type_local_id
GROUP BY dim_type_local.nom;
```

### Exercice

Maintenant que vous avez implémenté une première modélisation en étoile,
reprenez votre diagramme de modélisation de données établi précédemment et
implémentez le en adaptant la méthode ci-dessus :

- [ ] Créer et remplir toutes les tables de dimensions
- [ ] Créer et remplir la table de fait
- [ ] Adapter les requêtes SQL en utilisant des jointures entre la table de fait et les dimensions
