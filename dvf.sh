#!/bin/bash

set -ex

# download the source dataset
curl -o full.csv.gz "https://files.data.gouv.fr/geo-dvf/latest/csv/2021/full.csv.gz"

# extract the CSV file from the archive
gunzip -k "full.csv.gz"

# create and load the database
sqlite3 data/dvf.db < dvf.sql

# cleanup temporary files
rm full.csv.gz
rm full.csv
