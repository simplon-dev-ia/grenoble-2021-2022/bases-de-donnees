# Bases de données - Analyse de données avec Metabase

Cette quatrième activité a pour but de découvrir l'analyse de données à l'aide
d'un logiciel de _Business Intelligence_ (_BI_).

## Objectifs

- Découvrir un logiciel professionnel d'analyse de données
- Découvrir l'univers du domaine Business Intelligence (BI)
- Créer des tableaux de bords de visualisations de données

## Recherche d'information

- [ ] Qu'est-ce que le Business Intelligence (BI) ?
- [ ] Quels sont les logiciels BI les plus utilisés ?

## Installation Docker et Metabase

Le logiciel BI à utiliser pour cette activité est [Metabase](https://www.metabase.com).
Cette application prenant la forme d'un serveur web, il est plus simple de l'utiliser
avec [Docker](https://www.docker.com).

Avant de pouvoir commencer à explorer Metabase, il est nécessaire l'environnement
d'exécution Docker :

- [ ] Installer Docker :
```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

- [ ] Vérifier que Docker fonctionne :
```bash
docker --version
```

- [ ] Installer Docker Compose :
```bash
sudo curl \
    -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo curl \
    -L https://raw.githubusercontent.com/docker/compose/1.29.2/contrib/completion/bash/docker-compose \
    -o /etc/bash_completion.d/docker-compose
```

- [ ] Vérifier que Docker Compose fonctionne :
```bash
docker-compose --version
```

- [ ] Lancer Metabase avec `docker-compose` (vous pouvez regarder la configuration dans le fichier [`docker-compose.yml`](docker-compose.yml)):
```bash
sudo docker-compose up -d
sudo docker-compose ps
```

- [ ] Vérifier que Metabase tourne en ouvrant un navigateur à l'adresse http://localhost:3000 (cela peut prendre quelques minutes)

## Base de données

Pour des raisons de simplicité, nous allons utiliser une base de données SQLite.

- [ ] Installer la base de données SQLite dans le dossier [data](data)
    - Si vous avez encore à votre disposition la base de données des DVF créée lors de la première activité,
      la copier dans ce répertoire
    - Si vous souhaitez utiliser une nouvelle base de données, lancer le script [dvf.sh](dvf.sh):
```bash
bash dvf.sh
```

- [ ] Une fois la base SQLite placée au bon endroit, l'ajouter dans Metabase :
    - Cliquer sur "Settings" / "Admin" (en haut à droite)
    - Cliquer sur "Databases" (menu du haut)
    - Cliquer sur "Add database"
    - Remplir les champs :
        - Database type : "SQLite"
        - Name : "DVF"
        - Filename : "/data/dvf.db"
    - Cliquer sur "Data Model" (menu du haut)
    - Vérifier que les champs de la table "Dvf" sont dans les bons types de données
    - Cliquer sur "Settings" / "Exit admin" (en haut à droite)

## Exploration de données

Metabase permet d'explorer les données des bases connectées de façon interactive,
mais aussi en utilisant des requêtes SQL directement.

Un exemple pour analyser "le nombre de ventes par type de local" :

1. Cliquer sur "Ask a question" (en haut à gauche)
2. Sélectionner "Simple question"
3. Sélectionner la base de données "DVF"
4. Sélectionner la table "Dvf"
5. Ouvrir la section "Summarize"
6. Rechercher le champs "Type Local" et cliquer sur "+" / "Add Grouping"
7. Cliquer sur "Show Editor"
8. Cliquer sur "Sort" puis "Count" puis sur la flêche pour le mode de tri descendant
9. Cliquer sur "Visualize" et trouver la visualisation adaptée
10. Cliquer sur "Save" pour sauvegarder la visualisation et donner un titre, par exemple "Nombre de ventes par type de local"
11. Ajouter la visualisation à un tableau de bord (_dashboard_) lorsque demandé

Exercice :

- [ ] Analyser et obtenir les résultats des requêtes SQL avec la table `dvf` de [la première activité](01_initiation.md)
- [ ] Créer des visualisations avec les requêtes réalisées
- [ ] Créer un tableau de bord avec plusieurs visualisations créées
