CREATE TABLE dvf(
  "id_mutation" TEXT,
  "date_mutation" TEXT,
  "numero_disposition" TEXT,
  "nature_mutation" TEXT,
  "valeur_fonciere" REAL,
  "adresse_numero" TEXT,
  "adresse_suffixe" TEXT,
  "adresse_nom_voie" TEXT,
  "adresse_code_voie" TEXT,
  "code_postal" TEXT,
  "code_commune" TEXT,
  "nom_commune" TEXT,
  "code_departement" TEXT,
  "ancien_code_commune" TEXT,
  "ancien_nom_commune" TEXT,
  "id_parcelle" TEXT,
  "ancien_id_parcelle" TEXT,
  "numero_volume" TEXT,
  "lot1_numero" TEXT,
  "lot1_surface_carrez" REAL,
  "lot2_numero" TEXT,
  "lot2_surface_carrez" REAL,
  "lot3_numero" TEXT,
  "lot3_surface_carrez" REAL,
  "lot4_numero" TEXT,
  "lot4_surface_carrez" REAL,
  "lot5_numero" TEXT,
  "lot5_surface_carrez" REAL,
  "nombre_lots" INTEGER,
  "code_type_local" TEXT,
  "type_local" TEXT,
  "surface_reelle_bati" INTEGER,
  "nombre_pieces_principales" INTEGER,
  "code_nature_culture" TEXT,
  "nature_culture" TEXT,
  "code_nature_culture_speciale" TEXT,
  "nature_culture_speciale" TEXT,
  "surface_terrain" INTEGER,
  "longitude" REAL,
  "latitude" REAL
);

.mode csv
.separator ","
.import --skip 1 "full.csv" dvf

# si l'option --skip n'est pas disponible:
# .import '| tail -n +2 full.csv' dvf
