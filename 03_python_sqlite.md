# Bases de données - SQLite en Python

Cette troisième activité va se concentrer sur l'utilisation de SQLite avec Python.

Nous avons jusqu'ici manipulé les bases SQLite en ligne de commande avec le programme `sqlite3`.
Il est intéressant de pouvoir manipuler ces bases avec un langage de programmation
tel que Python afin de faciliter l'usage de bases au sein d'une application.

## Module `sqlite3`

La bibliothèque standard du langage Python intègre nativement le [module `sqlite3`][python-sqlite3]
permettant de manipuler des bases de données SQLite.

### Introduction

L'utilisation de ce module est relativement aisée :

- [ ] Importer le module `sqlite3` :
```python
import sqlite3
```
- [ ] Créer une connexion à une base SQLite (peut être un chemin vers un fichier ou en mémoire avec `:memory:`) :
```python
conn = sqlite3.connect("dvf.db")
```
- [ ] Pour pouvoir exécuter des requêtes SQL, il faut créer un _curseur_ :
```python
cur = conn.cursor()
```
- [ ] Exécuter une requête SQL avec le curseur, les résultats sont retournés sous la forme d'une **liste de tuples** :
```python
cur.execute(
    """
    SELECT DISTINCT type_local
    FROM dvf
    """
)
type_local_rows = cur.fetchall()
print(type_local_rows)
```
- [ ] Si la requête SQL effectue une modification dans la base (`INSERT`, `UPDATE`), il faut commiter les changements pour qu'ils soient effectifs :
```python
cur.execute("CREATE TABLE test(id INTEGER, name TEXT)")
cur.execute("INSERT INTO test(id, name) VALUES (1, 'John')")
conn.commit()
```
- [ ] Lorsque l'utilisation est terminée, fermer la connexion à la base :
```python
conn.close()
```

### Exercice

En reprenant la base de données `dvf.db` précédemment créée et à l'aide du module Python `sqlite3` :

- [ ] Implémenter et obtenir les résultats des requêtes SQL avec la table `dvf` de [la première activité](01_initiation.md)
- [ ] Implémenter les requêtes de transformation de la base vers un schéma en étoile comme dans [la seconde activité](02_modelisation_analytique.md)

## SQL avec `pandas`

Il est également possible d'utiliser le module Python `pandas` pour exécuter
des [requêtes SQL][pandas-readsql] et manipuler les données sous forme de _DataFrame_.

- [ ] Importer les modules `sqlite3` et `pandas`
```python
import sqlite3
import pandas as pd
```
- [ ] Créer une connexion à une base SQLite (peut être un chemin vers un fichier ou en mémoire avec `:memory:`) :
```python
conn = sqlite3.connect("dvf.db")
```
- [ ] Exécuter une requête SQL avec Pandas :
```python
pd.read_sql("select distinct type_local from dvf", conn)
```
- [ ] Lorsque l'utilisation est terminée, fermer la connexion à la base :
```python
conn.close()
```

### Exercice

En reprenant la base de données `dvf.db` précédemment créée et à l'aide des modules Python `sqlite3` et `pandas` :

- [ ] Implémenter et obtenir les résultats des requêtes SQL avec la table `dvf` de [la première activité](01_initiation.md)
- [ ] Afficher des graphes avec les _DataFrames_ obtenus

[python-sqlite3]: https://docs.python.org/3/library/sqlite3.html "Python - Module SQLite3"
[pandas-readsql]: https://pandas.pydata.org/docs/reference/api/pandas.read_sql.html "Pandas - read_sql"
